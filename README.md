# Artista

Artista is an experimental level editor with the aim of not being restricted to any engine, because of this he uses very general concepts.

For this, Artista is a level editor that uses a set of concepts and files in the form of a project that should be implemented in your preferred engine or framework.

Artista, through ECS, uses basic and specific components and systems to work (but it may be possible to make the systems optional, since in some engines such as Defold and Unity with MonoBehaviours, components are also responsible for the logic of the game).

The basic components support all the features of Artista, they are:

* Mesh;
* Text;
* Editable Text
* Editable Mesh
* Collider;
* Clickable;
* Local and Global Transform;
* Entity (this component will store information from the associated entity, because in ECS the entity itself will be an ID that will not contain data, so a component is required)

The specific components are the ones you will probably want to use, it is recommended to create your specific implementations, but if the implementation does not already exist, Artista can implement these components through the basic components in child entities; specific implementations of these specific components are recommended though.

The specific components are:

* Sprite Component (can be just a Mesh with texture);
* TileMap Component (can be a collection of Sprites)
* Some basic GUI components:
    * Button (Can be just a Clickable with Sprite and Text)
    * Text Entry Box (Can be just a Clickable, with Text and some Meshes)
    * Check box (Can be Clickable and Sprites for the boxes)

The purpose of this approach is to make Artista initially simple to implement, but with the possibility of creating the specific implementations of specific components and systems over time.

In addition, an Artista implementation must be able to read a project produced with Artista and then create the entities and their components specified in the project.

That way, if you use a framework or game engine, or created your own framework or engine, and would like to use the Artista editor, you just need to create an at least basic implementation, and thus have all the functionality of the editor at hand.

Interestingly, the editor itself is also an Artista project and will need to run in one implementation, so if the implementation is able to run the editor, then it will probably be able to run your games normally.

The Artista project also tries not to impose how the implementation should be done, it only requires that it obey the relation between the assets, entities, components, etc.

Keep in mind that all this is hypothetical and experimental, I'm not sure if it will really work, but I'm curious about the results.